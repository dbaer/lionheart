// $Id: FormulaRegistry.cc,v 1.4 2000/01/24 17:42:52 david Exp $

// FormulaRegistry.cc
// Keep track of extra generated formulae in order to facilitate
//  killing them off later on.

#include <iostream>
#include "FormulaRegistry.h"

using namespace std;

void FormulaRegistry::annihilate()
// destroy extra formulae -- broken at present (generates a SEGV)
{
   unsigned i;

   cout << "The following excess formulas will be deleted: " << endl;
   for (i = 0; i < list.size(); i++)
      list(i)->println(cout);
   if (!killed)
      for (i = 0; i < list.size(); i++)
      {
         cout << "deleting: " << flush;
         list(i)->println(cout);
         delete list(i);
         cout << "deleted." << endl;
      }

   killed = 1;
}

