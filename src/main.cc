// $Id: main.cc,v 1.5 2000/01/30 00:59:32 david Exp $

// main.cc
// by David Baer
//
// Main module -- read formula and attempt a proof, then save if desired

#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdlib>
//#include <bsd/string.h>
#include <cstring>
#include "formula.h"
#include "Proof.h"
#include "parser.h"
#include "Options.h"
#include "Axiom.h"
#include "ParamSource.h"

using namespace std;

int main(int argc, char *argv[])
{
   PropFormula *form;
   char filename[200];
   Options opt(argc, argv);

   if (opt.help())
   {
      opt.syntax();
      exit(0);
   }

   form = opt.getFormula();

   if (!form)
   {
      cout << "Formula to be proved: " << flush;

      initParser(cin);
      form = parseFormula();
      endParser();
   }

   if (!form)
   {
      cout << "not a valid formula" << endl;
      exit(0);
   }

   psource.setMax(opt.maxParam());

   //form->println(cout);
   RuleSource *guide;
   guide = (opt.manual() ? new UserRuleSource(cin, cout) : new AutoRuleSource(cin, cout, opt.quiet(), opt.pause(), opt.induction()));
   Proof *pf = (opt.manual() ? new Proof(form, guide) : new Proof(axioms.getVector(), form, guide));
   //Proof *pf = new Proof(form);
   if (pf->prove() == PROOF_TRUE)
   {
      if (!opt.quiet()) cout << "Theorem is a tautology" << endl;
      //cin >> filename;
      //cin.getline(filename, 198); // don't ask me why I have to do this...
      if (!opt.dump())
      {
         if (opt.outputFile())
         {
            if (!opt.quiet()) cout << "Saving proof to " << opt.outputFile() << endl;
            strlcpy(filename, opt.outputFile(), 200);
         }
         else
         {
            cout << "Save proof to <none>: " << flush;
            cin.getline(filename, 198);
         }
         if (strlen(filename))
         {
            ofstream o(filename);
            pf->print(o);
         }
      }
      else pf->print(cout);
   }
   else cout << "Did not find a proof " << endl;

   //pf->cleanup();
   delete pf;
   return 0;
}
