/* $Id: Axiom.cc,v 1.3 2000/01/24 17:42:52 david Exp $ */

/* Axiom.cc
   by David Baer, 8/14/99
   Routines for storing and indexing a list of axioms
*/

#include <iostream>
#include <fstream>
#include "Axiom.h"
#include "parser.h"

using namespace std;

#ifndef NULL
#define NULL 0
#endif

AxiomTable::AxiomTable(unsigned sz)
{
   tblSize = sz;
   data = new AxiomList[tblSize];
}

static inline
unsigned hashItem(char *key, unsigned modulus)
{
   unsigned result = 0, i;

   for (i = 0; i < strlen(key); i++)
      result = (256 * result + (unsigned)key[i]) % modulus;

   return result;
}

void AxiomTable::addAxiom(Axiom *ax)
{
   unsigned pos = hashItem(ax->getName(), tblSize);
   data[pos].addElement(ax);
}

void AxiomTable::addAxioms(char *filename)
{
   ifstream istr(filename);
   char inbuf[1024];

   cout << "Reading axioms from " << filename << "... " << flush;
   
   while (istr.getline(inbuf, 1022))
   {
      if (strlen(inbuf) && (inbuf[0] != '#'))
      {
         char *name = strtok(inbuf, ":"),
            *formstr = strtok(NULL, ":");

         cout << name << " " << flush;
         initParser(formstr);
         PropFormula *form = parseFormula();
         endParser();

         this->addAxiom(new Axiom(name, form));
      }
   }
   cout << endl;

}

void AxiomTable::printAxioms(ostream &o)
{
   unsigned i;

   for (i = 0; i < tblSize; i++)
   {
      unsigned j;
      for (j = 0; j < data[i].size(); j++)
      {
         o << data[i](j)->getName() << ": ";
         data[i](j)->getFormula()->println(o);
      }
   }
}

Vector<PropFormula*> AxiomTable::getVector()
{
   unsigned i;
   Vector<PropFormula*> result;

   for (i = 0; i < tblSize; i++)
   {
      unsigned j;
      for (j = 0; j < data[i].size(); j++)
         result.addElement(data[i](j)->getFormula());
   }

   return result;
}
      
PropFormula *AxiomTable::getAxiom(char *name)
{
   unsigned key = hashItem(name, tblSize),
      i;

   for (i = 0; i < data[key].size(); i++)
      if (!strcmp(data[key](i)->getName(), name))
         return data[key](i)->getFormula();

   cerr << "Error!  Couldn't find axiom!" << endl;
   return NULL;
}


AxiomTable axioms;

