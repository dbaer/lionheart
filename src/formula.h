// $Id: formula.h,v 1.4 2000/01/25 03:05:43 david Exp $

// formula.h
//
// classes defining propositional formulas

#ifndef _FORMULA_H
#define _FORMULA_H

#include <cstdio>
#include <iostream>
#include <string.h>
#include "Vector.h"
#include "Markable.h"

using namespace std;

#ifndef boolean
#define boolean int
#define true 1
#define false 0
#endif   /* !def _boolean */

// enumerated type for binary operators
typedef enum {
   BINOP_AND,
   BINOP_OR,
   BINOP_IMPL } BooleanOperator;

typedef enum {
   QUANT_FORALL,
   QUANT_EXISTS } QuantifierType;

class Var;
class Not;
class BinOp;
class ConstFormula;
class QuantFormula;

// supertype for propositional formulas, along with overridable
//  type-determining functions.  Markable superclass lets us hide irrelevant formulas
class PropFormula : public Markable
{
private:
   boolean tag;
   PropFormula *_parent;
public:
   // the following are pseudo-casts
   virtual boolean isVar() { return false; } // overridden by Var
   virtual Var *asVar() { return NULL; }

   virtual boolean isNot() { return false; } // overridden by Not
   virtual Not *asNot() { return NULL; }

   virtual boolean isBinOp() { return false; } // overridden by BinOp
   virtual BinOp *asBinOp() { return NULL; }

   virtual boolean isConst() { return false; } // overridden by ConstFormula
   virtual ConstFormula *asConst() { return NULL; }

   virtual boolean isQuant() { return false; }
   virtual QuantFormula *asQuant() { return NULL; }

   // printing routines
   virtual void print(ostream&) = 0;
   void println(ostream&);

   // equivalence testing routine
   virtual boolean equals(PropFormula *) = 0;

   // copy formula
   virtual PropFormula * formCopy() = 0;

   // instantiating quantified formulas requires...
   virtual PropFormula * replace(Var * /* current */, Var * /* replacement */) = 0;

   // tag manipulation
   void setTag() { tag = true; }
   boolean tagged() { return tag; }

   // parent formula
   void setParent(PropFormula *p) { _parent = p; }
   PropFormula* getParent() { return _parent; }

   PropFormula() : Markable(0) { tag = false; _parent = NULL; }
   
};

class Pred;

// a formula consisting entirely of a single variable
class Var : virtual public PropFormula
{
  char *name;
public:
  Var(const char *n) { name = strdup(n); }

   virtual void print(ostream&);

   virtual boolean equals(PropFormula *);

   // override the right pseudo-casts
   Var *asVar() { return this; }
   boolean isVar() { return true; }

   virtual boolean isPred() { return false; }
   virtual Pred *asPred() { return NULL; }

   // accessor for variable name
   char *varName() { return name; }

   virtual PropFormula * formCopy() 
      {
         PropFormula *result = new Var(name);

         // set parent
         result->setParent(this);
         if (tagged()) result->setTag();

         return result;
      }

   virtual PropFormula * replace(Var *, Var *);
};

class Pred : virtual public Var
{
  Vector<Var*> params;
public:
  Pred(char *n, Vector<Var*> p) : Var(n) { params = copy(p); }

  virtual void print(ostream &);
  virtual boolean equals(PropFormula *);

  boolean isPred() { return true; }
  Pred *asPred() { return this; }

  Vector<Var*> &getParams() { return params; }

  PropFormula * formCopy () 
     {
        PropFormula *result = new Pred(this->varName(), copy(params));

        result->setParent(this);
        if (tagged()) result->setTag();

        return result;
     }

  PropFormula * replace(Var *, Var *);

};

// a formula consisting of the negation of another formula
class Not : virtual public PropFormula
{
   PropFormula *operand;
public:
   Not(PropFormula *opnd) { operand = opnd; }
   void print(ostream&);
   boolean equals(PropFormula *);

   // override the appropriate pseudo-casts
   Not *asNot() { return this; }
   boolean isNot() { return true; }

   // accessor for operand of 'not' operator
   PropFormula *notOperand() { return operand; }

   PropFormula * formCopy () 
      {
         PropFormula *result = new Not(operand->formCopy());

         result->setParent(this);
         if (tagged()) result->setTag();

         return result;
      }

   PropFormula * replace(Var *v, Var *r) 
      {
         PropFormula *result = new Not(operand->replace(v, r));

         result->setParent(this);
         if (tagged()) result->setTag();

         return result;
      }
};

// a formula encoding a binary operation on two subformulas
class BinOp : virtual public PropFormula
{
   PropFormula *left, *right;
   BooleanOperator opr;
public:
   BinOp(BooleanOperator o, PropFormula *l, PropFormula *r)
   {
      left = l;
      right = r;
      opr = o;
   }

   boolean isBinOp() { return true; }
   BinOp *asBinOp() { return this; }

   void print(ostream&);

   boolean equals(PropFormula *);

   PropFormula *leftOperand() { return left; }
   PropFormula *rightOperand() { return right; }

   BooleanOperator op() { return opr; }

   PropFormula * formCopy () 
      {
         PropFormula *result = new BinOp(opr, left->formCopy(), right->formCopy());

         result->setParent(this);
         if (tagged()) result->setTag();

         return result;
      }

   PropFormula * replace(Var *v, Var *r) 
      {
         PropFormula *lf = left->replace(v, r),
            *rf = right->replace(v, r),
            *result = new BinOp(opr, lf, rf);

         result->setParent(this);
         if (tagged()) result->setTag();

         return result;
      }
};

class ConstFormula : virtual public PropFormula
{
   boolean val;
public:
   ConstFormula(boolean v)
   {
      val = v;
   }

   boolean value() { return val; }

   boolean isConst() { return true; }
   ConstFormula *asConst() { return this; }

   void print(ostream&);

   boolean equals(PropFormula *);

   PropFormula * formCopy () 
      {
         PropFormula *result = new ConstFormula(val);

         result->setParent(this);
         if (tagged()) result->setTag();

         return result;
      }

   PropFormula * replace(Var *, Var *) { return this->formCopy(); }
};


class QuantFormula : virtual public PropFormula
{
   QuantifierType type;
   Var *variable;
   PropFormula *formula;
   unsigned nextP;

   PropFormula *instantForm; // keep track of instantiated formula
public:
   QuantFormula(QuantifierType t, Var *v, PropFormula *f, unsigned nP = 0)
   {
      type = t;
      variable = v;
      formula = f;
      nextP = nP;
      instantForm = NULL;
   }

   boolean isQuant() { return true; }
   QuantFormula *asQuant() { return this; }

   void print(ostream&);
   boolean equals(PropFormula *);

   QuantifierType getType() { return type; }
   Var *getVariable() { return variable; }
   PropFormula *getFormula() { return formula; }

   unsigned nextParam() { return nextP; }
   void incParam() { nextP++; }
   void decParam() { nextP--; }
   PropFormula *instantiate(Var *);

   PropFormula *instantFormula() { return instantForm; }

   PropFormula * formCopy () 
      {
         PropFormula *result = new QuantFormula(type, variable->formCopy()->asVar(), formula->formCopy(), nextP);

         result->setParent(this);
         if (tagged()) result->setTag();

         return result;
      }
   
   PropFormula * replace(Var *, Var *);
};

#endif   /* !_FORMULA_H */

