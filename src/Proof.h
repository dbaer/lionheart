// $Id: Proof.h,v 1.3 2000/01/24 17:42:52 david Exp $

// Proof.h
// Proof construct

#ifndef _PROOF_H
#define _PROOF_H

#include "Rule.h"
#include "RuleSource.h"
#include "Vector.h"
#include "FormulaRegistry.h"
#include "formula.h"

// sometimes C++ can be braindead...
#ifndef NULL
#define NULL 0
#endif   /* !def NULL */

// enumerated type to encode proof results
typedef enum
{
   PROOF_FALSE = 0,
   PROOF_TRUE,
   PROOF_BACKUP
} ProofReturnCode;

class Proof
{
   Vector<PropFormula*> lhs;       // the lefthand side of the turnstyle
   PropFormula          *rhs;      // the righthand side -- a single formula
   Proof                *child1,   // proof can have 2 subgoals
                        *child2;
   RuleSource           *guide;    // this abstraction supplies the rules
   Rule                 step;      // the step taken at this stage (from guide)
   FormulaRegistry      *reg;      // the global registry of extra formulae
public:

   // constructor (for outside use) -- setup proof for a single formula
   Proof(PropFormula *f, RuleSource *g, FormulaRegistry *r = NULL);

   // constructor (for internal use) -- setup a proof subgoal
   Proof(Vector<PropFormula*>l, PropFormula *f, RuleSource *g,
         FormulaRegistry *r = NULL);

   // apply a given rule
   int applyRule(Rule); // 0 = invalid rule, 1 = valid rule

   // try to prove this seqent with rules from guide
   ProofReturnCode prove();

   // print this sequent
   void print(ostream& o, int indent = 0);
   void println(ostream& o, int indent = 0)
   {
      print(o, indent);
      o << endl;
   }

   void cleanup(); // delete excess formulas

   // class destructor
   ~Proof();
};

#endif   /* !_PROOF_H */

