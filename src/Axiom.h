/* $Id: Axiom.h,v 1.3 2000/01/24 17:42:52 david Exp $ */

/* Axiom.h
   by David Baer, 8/14/99
   Routines for storing and indexing a list of axioms
*/

#ifndef _AXIOM_H
#define _AXIOM_H

#include <iostream>
#include "formula.h"
#include "Vector.h"

using namespace std;

class Axiom
{
   char *name;
   PropFormula *formula;
 public:
   Axiom(char *n, PropFormula *f) { name = strdup(n); formula = f; }

   char *getName() { return name; }
   PropFormula *getFormula() { return formula; }
};

typedef Vector<Axiom*> AxiomList;

/* store in hash table, resolve collisions by chaining */
class AxiomTable
{
   AxiomList *data;
   unsigned tblSize;
 public:
   AxiomTable(unsigned sz = 100);

   void addAxiom(Axiom *);
   void addAxioms(char * /* filename */);
   
   PropFormula *getAxiom(char * /* name */);

   Vector<PropFormula*> getVector();

   void printAxioms(ostream &);
};

extern
AxiomTable axioms;

#endif   /* !def _AXIOM_H */
