/* $Id: Proof.cc,v 1.9 2000/01/29 22:33:53 david Exp $ */

/* Proof.cc
   by David Baer

   Routines for object that applies rules as it is given them
*/

#include <iostream>
#include "Proof.h"
#include "ParamSource.h"
#include "Axiom.h"
#include "copysplit.h"

using namespace std;

// constructor (for outside use)
Proof::Proof(PropFormula *f, RuleSource *g, FormulaRegistry *r)
{
   child1 = child2 = NULL;   // initialize subgoals to NULL
   rhs = f;                  // set user-specified RHS of sequent
   guide = g;                // initialize guide

   // if not specified, set up new formula registry
   if (!r)
      reg = new FormulaRegistry();
   else reg = r;
}

// for outside use -- takes explicit LHS in addition to other params
Proof::Proof(Vector<PropFormula*>l, PropFormula *f, RuleSource *g,
      FormulaRegistry *r)
{
   child1 = child2 = NULL;
   rhs = f;
   lhs = l;
   guide = g;
   if (!r)
      reg = new FormulaRegistry();
   else reg = r;
}

static inline boolean
rightRule(RuleType r)
{
   return (r == AND_R ||
           r == OR_R1 ||
           r == OR_R2 ||
           r == IMP_R ||
           r == NOT_R ||
           r == MAGIC ? true : false);
}


ProofReturnCode Proof::prove()
{
   int ruleResult = 0;

   // the following construction requires some explanation:
   //  we loop endlessly until a "return" is encountered --
   //  the only way we can loop is if one of the subgoals returns
   //  PROOF_BACKUP, meaning that the guide wishes to try a different
   //  rule for this sequent.  Hence the 'while(1)'
   while (1)
   {
      do // loop until an appropriate rule can be applied
      {
         step = guide->getRule(lhs, rhs);
         if (step.getRule() == BACKUP_RULE)
            return PROOF_BACKUP;
         ruleResult = applyRule(step);
         // ruleResult = 1 if rule successfully applied, 0 otherwise
      }
      while (!ruleResult);

      // did not find proof
      if (ruleResult == -1)
         return PROOF_FALSE;
      else if (ruleResult == -2) // found proof
         return PROOF_TRUE;
   
      // solve subgoals, if applicable
      if (child1)
      {
         ProofReturnCode result1 = child1->prove();

         // if backing up from subgoal, delete failed subgoal and
         //  re-try this sequent
         if (result1 == PROOF_BACKUP)
         {
            delete child1;
            child1 = NULL;
            if (child2) { delete child2; child2 = NULL; }
            continue;
         }
         else if (result1 == PROOF_TRUE)
            if (child2)
            {
               // first, let's check to see whether the step that
               //  gave us two subgoals was superfluous.  If it was,
               //  then we don't need to check subgoal 2, since it
               //  won't show up when we print the proof.
               if ((step.getTarget() && !step.getTarget()->tagged()) ||
                   (!rhs->tagged() && rightRule(step.getRule())))
                  return PROOF_TRUE;

               // otherwise, we're out of luck -- we have to prove the
               //  second subgoal...
               ProofReturnCode result2 = child2->prove();
               if (result2 == PROOF_BACKUP)
               {
                  delete child1;
                  child1 = NULL;
                  delete child2;
                  child2 = NULL;
                  continue;
               }
               else if (result2 == PROOF_TRUE)
                  return PROOF_TRUE;    // child1 & child2 are true
               else return PROOF_FALSE; // child1 true, child2 false
            }
            else return PROOF_TRUE; // child1 true
         else return PROOF_FALSE;   // child1 false
      }
      else return PROOF_TRUE;       // no children -- "hyp" applied ==> true
   }
}

// print out the proof, complete with sequents and rules
void Proof::print(ostream& o, int indent)
{
   boolean superfluous = ((step.getTarget() &&
      (!step.getTarget()->tagged() ||
       (step.getTarget()->isQuant() && !step.getTarget()->asQuant()->instantFormula()->tagged())
      )) ||
      (!rhs->tagged() && rightRule(step.getRule())) ?
                          true : false),
           printed = false; // set to true once something is printed on a line

   if (!reg)
   {
      o << "error! excess formulas have been cleaned!" << endl;
      return;
   }

   if (!superfluous)
   {
      // do indentation
      for (int i = 0; i < indent; i++)
         o << " ";
   
      // print left-hand formulae
      for (unsigned i = 0; i < lhs.size(); i++)
      {
         if (lhs(i)->tagged())
         {
            lhs(i)->print(o);
            printed = true;
         }
         if (printed && (i < lhs.size() - 1) && lhs(i + 1)->tagged())
            o << ", ";
      }
      o << " |- ";
   
      // print right-hand formula
      if (rhs->tagged())
         rhs->print(o);
      else o << "false";
   
      // print rule used here
      RuleType type = step.getRule();
      o << " by " << rulename[(int)type];
   
      // this commented-out section doesn't work for some reason...
      // ... it is an attempt to show which left-hand formulae were
      //     manipulated by each of the following rules
      /*if ((type == AND_L) ||
          (type == OR_L) ||
          (type == IMP_L) ||
          (type == NOT_L))
         o << " " << step.argument() + 1;*/
   
      // if we cut in a formula, say what it is!
      if (type == CUT)
      {
         o << " ";
         step.getTarget()->print(o);
      }
   
      o << endl;
   }

   // if there are two subgoals, set them off with indentation
   if (child1 && child2 &&
       !superfluous         // if this step is superflous, we don't need to
                            //  see 2 subgoals (no info gained)
      )
   {
      child1->print(o, indent + 3);
      o << endl;
      child2->print(o, indent + 3);
   }
   // otherwise, just continue with the single subgoal
   else if (child1)
      child1->print(o, indent);
}

static inline
QuantFormula *flipQuant(QuantFormula *qf)
// return the logical negation of a quantified formula, with the ~ on the INSIDE
{
   PropFormula *inside;
   
   if (qf->getFormula()->isQuant())
      inside = flipQuant(qf->getFormula()->asQuant());
   else if (qf->getFormula()->isNot())
      inside = qf->getFormula()->asNot()->notOperand();
   else inside = new Not(qf->getFormula());

   return new QuantFormula(qf->getType() == QUANT_FORALL ? QUANT_EXISTS : QUANT_FORALL,
                           qf->getVariable(), inside);
}


// attempt to apply the given rule to the sequent
int Proof::applyRule(Rule r)
{
   PropFormula *target = r.getTarget();   // the target of the rule
                                          // (may be a new formula [CUT]
                                          //  or an old one [AND_L, etc.]
   BinOp *binop = NULL,                   // binop version of rhs
         *binopt = NULL;                  // binop version of target

   if (r.isProof()) // rule supplies a proof
   {
      // give it the benefit of the doubt...
      child1 = r.getProof();
      return -2;
   }

   // do the appropriate pseudo-casts for binary expressions
   //  for easier access below...
   if (rhs->isBinOp())
      binop = rhs->asBinOp(); 
   if (target)
   {
      if (target->isBinOp())
         binopt = target->asBinOp();
   }

   // multiplex on rule type
   switch (r.getRule())
   {
      case AND_L : if (binopt && (binopt->op() == BINOP_AND))
                   {
                      Vector<PropFormula*> new_lhs = copysplitbut(lhs, target);
                      new_lhs.addElement(binopt->leftOperand());
                      new_lhs.addElement(binopt->rightOperand());

                      // set parent info
                      binopt->leftOperand()->setParent(binopt);
                      binopt->rightOperand()->setParent(binopt);

                      child1 = new Proof(new_lhs, rhs, guide, reg);
                      return 1;
                   }
                   break;
      case AND_R : if (binop && (binop->op() == BINOP_AND))
                   {
                      // set parent info
                      binop->leftOperand()->setParent(binop);
                      binop->rightOperand()->setParent(binop);

                      child1 = new Proof(copysplit(lhs), binop->leftOperand(),
                                         guide, reg);
                      child2 = new Proof(copysplit(lhs), binop->rightOperand(),
                                         guide, reg);
                      return 1;
                   }
                   break;
      case OR_L  : if (binopt && (binopt->op() == BINOP_OR))
                   {
                      Vector<PropFormula*> new_lhs1 = copysplitbut(lhs, target),
                                           new_lhs2 = copysplit(new_lhs1);
                      new_lhs1.addElement(binopt->leftOperand());
                      new_lhs2.addElement(binopt->rightOperand());
                      child1 = new Proof(new_lhs1, rhs, guide, reg);
                      child2 = new Proof(new_lhs2, rhs, guide, reg);

                      // set parent info
                      binopt->leftOperand()->setParent(binopt);
                      binopt->rightOperand()->setParent(binopt);

                      return 1;
                   }
                   break;
      case NOT_L : {
                      Vector<PropFormula*> new_lhs = copysplitbut(lhs, target);
                      PropFormula *new_rhs;
         
                      if (target->isNot())
                         new_rhs = target->asNot()->notOperand();
                      else if (target->isQuant())
                      {
                         /*PropFormula *inside = target->asQuant()->getFormula();
                         new_rhs = new QuantFormula(target->asQuant()->getType() == QUANT_FORALL ? QUANT_EXISTS : QUANT_FORALL,
                                                    target->asQuant()->getVariable(),
                                                    inside->isNot() ? inside->asNot()->notOperand() : new Not(inside));*/
                         new_rhs = flipQuant(rhs->asQuant());
                      }
                      else new_rhs = new Not(target);

                      // set parent info
                      new_rhs->setParent(target);
                      
                      child1 = new Proof(new_lhs, new_rhs, guide, reg);
                      return 1;
                   }
                   break;
      case NOT_R : {
                      Vector<PropFormula*> new_lhs = copysplit(lhs);
                      PropFormula *new_elt;

                      if (rhs->isNot())
                         new_elt = rhs->asNot()->notOperand();
                      else if (rhs->isQuant())
                      {
                         new_elt = flipQuant(rhs->asQuant());
                      }
                      else new_elt = new Not(rhs);

                      // set parent info
                      new_elt->setParent(rhs);
                      
                      new_lhs.addElement(new_elt);
                      child1 = new Proof(new_lhs, new ConstFormula(false), guide, reg);
                      return 1;
                   }
                   break;
      case IMP_L : if (binopt && (binopt->op() == BINOP_IMPL))
                   {
                      Vector<PropFormula*> new_lhs1 = copysplitbut(lhs, target),
                                           new_lhs2 = copysplit(new_lhs1);
                      new_lhs2.addElement(binopt->rightOperand());

                      // set parent info
                      binopt->leftOperand()->setParent(binopt);
                      binopt->rightOperand()->setParent(binopt);

                      child1 = new Proof(new_lhs1, binopt->leftOperand(),
                                         guide, reg);
                      child2 = new Proof(new_lhs2, rhs, guide, reg);
                      return 1;
                   }
                   break;
      case OR_R2 :
      case OR_R1 : if (binop && (binop->op() == BINOP_OR))
                   {
                      // set parent info
                      PropFormula *left = binop->leftOperand()->formCopy(),
                                  *right = binop->rightOperand()->formCopy();

                      cout << "holla!" << endl;

                      left->setParent(binop);
                      right->setParent(binop);

                      cout << "left:  ";
                      left->print(cout);
                      cout << " [";
                      left->getParent()->print(cout);
                      cout << "]" << endl;
                      cout << "right: ";
                      right->print(cout);
                      cout << " [";
                      right->getParent()->print(cout);
                      cout << "]" << endl;

                      child1 = new Proof(copysplit(lhs), (r.getRule() == OR_R1 ?
                                                       left
                                                     : right),
                                         guide, reg);
                      return 1;
                   }
                   break;
      case OR_R  : if (binop && (binop->op() == BINOP_OR))
                   {
                      Vector<PropFormula*> new_lhs = copysplit(lhs);
                      PropFormula *new_elt = binop->leftOperand();

                      if (new_elt->isNot())
                         new_elt = new_elt->asNot()->notOperand();
                      else if (new_elt->isQuant())
                      {
                         new_elt = flipQuant(new_elt->asQuant());
                      }
                      else new_elt = new Not(new_elt);

                      // set parent info
                      new_elt->setParent(binop);

                      new_lhs.addElement(new_elt);

                      binop->rightOperand()->setParent(binop);

                      child1 = new Proof(new_lhs, binop->rightOperand(),
                                         guide, reg);

                      return 1;
                   }
                   break;
      case IMP_R : if (binop && (binop->op() == BINOP_IMPL))
                   {
                      Vector<PropFormula*> new_lhs = copysplit(lhs);
                      new_lhs.addElement(binop->leftOperand());

                      // set parent info
                      binop->leftOperand()->setParent(binop);
                      binop->rightOperand()->setParent(binop);

                      child1 = new Proof(new_lhs, binop->rightOperand(),
                                         guide, reg);
                      return 1;
                   }
                   break;
      case HYP   : {
                      unsigned i, result = 0;
                      for (i = 0; !result && (i < lhs.size()); i++)
                         if (lhs(i)->equals(rhs))
                         {
                            result = 1;

                            // set tags on ancestors (this is a closed branch)
                            PropFormula *tempPtr = lhs(i);
                            // cout << "Setting tags on ancestors ... " << endl;
                            while (tempPtr && !tempPtr->tagged())
                            {
                               // cout << "* ";
                               // tempPtr->println(cout);
                               tempPtr->setTag();
                               tempPtr = tempPtr->getParent();
                            }

                            tempPtr = rhs;
                            while (tempPtr && !tempPtr->tagged())
                            {
                               // cout << "* ";
                               // tempPtr->println(cout);
                               tempPtr->setTag();
                               tempPtr = tempPtr->getParent();
                            }
                            // cout << endl;
                         }
                      return result;
                   }
      case GIVEUP: return -1;
      case INSTANT:if (target->isQuant())
                   {
                      QuantFormula *qform = target->asQuant();
                      Vector<PropFormula*> new_lhs;

                      if (qform->getType() == QUANT_FORALL)
                      {
                         /* cout << "instantiated formula: " << flush;*/
                         char paramstr[20];
                         snprintf(paramstr, 20, "p%u", r.argument());
                         
                         PropFormula *new_elt = qform->instantiate(new Var(paramstr));
                         /* new_elt->println(cout); */

                         /* cout << "old formula: " << flush;
                         qform->println(cout); */
                         qform->incParam();

                         // need to ADD a formula to the list -- can use any parameter
                         new_lhs = copysplit(lhs);
                         new_lhs.addElement(new_elt);

                         // set parent info
                         new_elt->setParent(qform);

                         //qform->decParam(); // safe after copy
                      }
                      else // qform->getType() == QUANT_EXISTS
                      {
                         // use fresh parameter
                         new_lhs = copysplitbut(lhs, target);

                         PropFormula *new_elt = qform->instantiate(psource.freshParam());

                         // set parent info
                         new_elt->setParent(qform);

                         new_lhs.addElement(new_elt);
                      }

                      child1 = new Proof(new_lhs, rhs, guide, reg);
                      return 1;
                   }
                   break;
      case AXIOM : {
                      Vector<PropFormula*> new_lhs = copysplit(lhs);
                      PropFormula *new_ax = target;

                      if (new_ax)
                      {
                         new_lhs.addElement(new_ax);
                         child1 = new Proof(new_lhs, rhs, guide, reg);
                         return 1;
                      }
                      else return 0;
                   }
      case IND   : {
                      Vector<PropFormula*> new_lhs1 = copysplit(lhs),
                                           new_lhs2 = copysplit(lhs);
                      PropFormula *zero = r.getInductionZero(),
                                  *succ = r.getInductionSucc(),
                                  *new_rhs1, *new_rhs2;
                      Var *iV = new Var("i"), *jV = new Var("j");
                      Var *zeroV = psource.freshParam(),
                          *lastV = psource.freshParam(),
                          *nextV = psource.freshParam();
                     
                      if (!zero || !succ)
                         cout << "Still not fixed!" << endl;

                      zero->println(cout);
                      succ->println(cout);

                      if (zero && succ && rhs->isQuant() && (rhs->asQuant()->getType() == QUANT_FORALL))
                      {
                         new_lhs1.addElement(zero->replace(iV, zeroV));
                         new_lhs2.addElement(succ->replace(iV, lastV)->
                                             replace(jV, nextV));
                         delete iV;
                         delete jV;
                         new_rhs1 = rhs->asQuant()->instantiate(zeroV);
                         new_rhs2 = rhs->asQuant()->instantiate(nextV);
                         new_lhs2.addElement(rhs->asQuant()->instantiate(lastV));

                         child1 = new Proof(new_lhs1, new_rhs1, guide, reg);
                         child2 = new Proof(new_lhs2, new_rhs2, guide, reg);

                         return 1;
                      }
                      else return 0;
                   }
      case CUT   : {
                      Vector<PropFormula*> new_lhs = copysplit(lhs);
                      reg->addFormula(target);
                      new_lhs.addElement(target);
                      child1 = new Proof(target, guide, reg);
                      child2 = new Proof(new_lhs, rhs, guide, reg);
                      return 1;
                   }
      case MAGIC : if (rhs->isBinOp() && (rhs->asBinOp()->op() == BINOP_OR))
                   {
                      BinOp *formula = rhs->asBinOp();
                      if (formula->rightOperand()->isNot())
                         return formula->rightOperand()->asNot()->
                                notOperand()->equals(formula->leftOperand());
                      else if (formula->leftOperand()->isNot())
                         return formula->leftOperand()->asNot()->
                                notOperand()->equals(formula->rightOperand());

                      // set tags on ancestors (this is a closed branch)
                      PropFormula *tempPtr = rhs;
                      while (tempPtr)
                      {
                         tempPtr->setTag();
                         tempPtr = tempPtr->getParent();
                      }
                   }
                   break;
      default : cerr << "Rule " << rulename[(int)r.getRule()]
                     << "not supported yet!" << endl;
   }
   return 0;
}

void Proof::cleanup()
{
   if (child1)
      delete child1;
   if (child2)
      delete child2;
   if (reg)
   {
      reg->annihilate();
      delete reg;
      reg = NULL;
   }
}

Proof::~Proof()
{
   if (child1)
      delete child1;
   if (child2)
      delete child2;
}

