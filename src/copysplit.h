#ifndef _COPYSPLIT_H
#define _COPYSPLIT_H

#include "Vector.h"
#include "formula.h"

extern Vector<PropFormula*>
copysplit(Vector<PropFormula*>);

extern Vector<PropFormula*>
copysplitbut(Vector<PropFormula*>, PropFormula*);

#endif  /* !def _COPYSPLIT_H */
