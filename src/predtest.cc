// $Id: predtest.cc,v 1.2 1999/11/17 01:47:34 david Exp $

// predtest.cc
// by David Baer
//
// Silly little test of predicates

#include <iostream.h>
#include <stdio.h>
#include "formula.h"
#include "Vector.h"

int main()
{
  char c;
  Vector<Var*> params;

  for (c = 'w'; c <= 'z'; c++)
    {
      char str[55] = "\0\0";
      //snprintf(str, 55, "%c", c);
      str[0] = c;
      params.addElement(new Var(str));
    }
  Pred *test = new Pred("P", params), *test2 = new Pred("Q", params);

  cout << "Preds:" << endl;
  test->println(cout);
  test2->println(cout);

  test->print(cout);
  if (test->equals(test2)) cout << "=";
  else cout << "!=";
  test2->print(cout);
  cout << endl;

  QuantFormula *test3 = new QuantFormula(QUANT_FORALL, new Var("w"), 
			new QuantFormula(QUANT_FORALL, new Var("x"),
			new QuantFormula(QUANT_FORALL, new Var("y"),
                        new QuantFormula(QUANT_FORALL, new Var("z"),
                                         test))));

  test3->println(cout);

  PropFormula *test4 = test3->instantiate(new Var("w0"));

  test4->println(cout);
  
  return 0;
}
