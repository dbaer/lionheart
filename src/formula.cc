// $Id: formula.cc,v 1.5 2000/01/25 03:05:43 david Exp $

// formula.cc
//
// mostly print methods for formula classes
#include <stdio.h>
#include "formula.h"

void PropFormula::println(ostream & o)
{
   print(o);
   o << endl;
}

void Var::print(ostream & o)
{
   o << name;
}

boolean Var::equals(PropFormula *formula)
{
   if (formula->isVar() == false)
      return false;
   Var *v = formula->asVar();
   if (!strcmp(v->name, name)) return true;
   return false;
}

PropFormula *Var::replace(Var *v, Var *r)
{
   if (this->equals(v))
   {
      PropFormula *result = r->formCopy();

      result->setParent(this);

      return result;
   }
   else return this->formCopy();
}

void Pred::print(ostream & o)
{
   if (!strcmp(this->varName(), "Equal") && (getParams().size() == 2))
   {
      o << "(";
      getParams()(0)->print(o);
      o << " = ";
      getParams()(1)->print(o);
      o << ")";
   }
   else
   {
      Var::print(o);
      o << "(";
      unsigned i;
      
      for (i = 0; i < getParams().size(); i++)
      {
         getParams()(i)->print(o);
         if (i < getParams().size() - 1)
            o << ", ";
      }
      o << ")";
   }
}

boolean Pred::equals(PropFormula *formula)
{
  if (Var::equals(formula) == false)
    return false;
  if (formula->asVar()->isPred() == false)
    return false;
  Pred *pf = formula->asVar()->asPred();

  if (pf->getParams().size() != getParams().size())
    return false;

  unsigned i;

  for (i = 0; i < getParams().size(); i++)
    if (!getParams()(i)->equals(pf->getParams()(i)))
      return false;

  return true;
}

PropFormula * Pred::replace(Var *v, Var *r)
{
   unsigned i;
   Pred *temp = this->formCopy()->asVar()->asPred();

   for (i = 0; i < temp->getParams().size(); i++)
      if (temp->getParams()(i)->equals(v))
      {
         //delete temp->getParams()(i);
         temp->getParams()(i) = r;
      }

   temp->setParent(this);

   return temp;
}

void Not::print(ostream & o)
{
   o << "~";
   operand->print(o);
   o << flush;
}

boolean Not::equals(PropFormula *formula)
{
   if (formula->isNot() == false)
      return false;
   Not *v = formula->asNot();
   return operand->equals(v->operand);
}

void BinOp::print(ostream & o)
{
   o << "(";
   if ((opr == BINOP_AND) &&
       left->isBinOp() &&
       (left->asBinOp()->op() == BINOP_IMPL) &&
       right->isBinOp() &&
       (right->asBinOp()->op() == BINOP_IMPL) &&
       (right->asBinOp()->leftOperand()->equals(left->asBinOp()->rightOperand())) &&
       (right->asBinOp()->rightOperand()->equals(left->asBinOp()->leftOperand())))
   {
      left->asBinOp()->leftOperand()->print(o);
      o << " <==> ";
      left->asBinOp()->rightOperand()->print(o);
   }
   else
   {
      left->print(o);
      switch(opr)
      {
         case BINOP_IMPL : o << " ==> ";
                           break;
         case BINOP_AND  : o << " & ";
                           break;
         case BINOP_OR   : o << " | ";
      }
      right->print(o);
   }

   o << ")" << flush;
}

boolean BinOp::equals(PropFormula *formula)
{
   if (formula->isBinOp() == false)
      return false;
   BinOp *v = formula->asBinOp();
   return left->equals(v->left) && right->equals(v->right);
}

void ConstFormula::print(ostream& o)
{
   o << (val ? "true" : "false") << flush;
}

boolean ConstFormula::equals(PropFormula *formula)
{
   if (formula->isConst() == false)
      return false;
   ConstFormula *v = formula->asConst();
   return (v->value() == value());
}

void QuantFormula::print(ostream & o)
{
   switch (type)
   {
   case QUANT_FORALL:
     o << "FORALL"; break;
   case QUANT_EXISTS:
     o << "EXISTS"; break;
   default:
     o << "?QUANT";
   }
   o << "{";
   variable->print(o);

   PropFormula *f = formula;

   while (f->isQuant() && (f->asQuant()->getType() == this->getType()))
   {
      o << ", ";
      f->asQuant()->getVariable()->print(o);
      f = f->asQuant()->getFormula();
   }
   
   o << "}";
   f->print(o);
   o << flush;
}

boolean QuantFormula::equals(PropFormula *form)
{
  if (form->isQuant() == false)
    return false;
  if (form->asQuant()->getType() != getType())
    return false;
  if (!form->asQuant()->getVariable()->equals(variable))
    return false;
  if (!form->asQuant()->getFormula()->equals(formula))
    return false;

  return true;
}

PropFormula * QuantFormula::instantiate(Var *sub)
{
   instantForm = formula->replace(variable, sub);
   return instantForm;
}

PropFormula * QuantFormula::replace(Var *v, Var *r)
{
   if (!this->variable->equals(v))
   {
      /* handle quantifier capture -- shouldn't happen if we're well-formed */


      PropFormula *result = new QuantFormula(this->type, this->variable->formCopy()->asVar(),
                              formula->replace(v, r));

      result->setParent(this);

      return result;
   }
   else return this->formCopy();
}

