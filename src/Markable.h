/* MarkedFormula.h
   by David Baer, 7/4/2000

   Allows us to mark formulas that persist from line 1 to line X of proof chain
*/

#ifndef _MARKABLE_H
#define _MARKABLE_H

class Markable
{
   unsigned long int end;
 public:
   Markable(unsigned long int n = 0) 
      {
         end = n;
      }

   void setEnd(unsigned long int n)
      {
         end = n;
      }
};

#endif   /* !def _MARKABLE_H */

