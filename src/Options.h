/* $Id: Options.h,v 1.5 2000/01/30 00:59:32 david Exp $ */

/* Options.h
   by David Baer

   Parse command-line options and convert to and object
*/

#ifndef _OPTIONS_H
#define _OPTIONS_H

#include "formula.h"
#include "Vector.h"

class Options
{
   int man_mode, quiet_mode, help_req, dump_mode, pause_mode,
       use_ind;
   unsigned max_param;
   PropFormula *form;
   char *output_file;
public:
   Options(int argc, char *argv[]);
   void syntax();
   PropFormula *getFormula() { return form; }
   int pause() { return pause_mode; }
   int manual() { return man_mode; }
   int quiet() { return quiet_mode; }
   int help() { return help_req; }
   int dump() { return dump_mode; }
   int induction() { return use_ind; }
   char *outputFile() { return output_file; }
   unsigned maxParam() { return max_param; }
   
};

#endif    /* !def _OPTIONS_H */

