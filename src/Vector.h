// $Id: Vector.h,v 1.3 2000/01/24 17:42:52 david Exp $

// Vector.h
// A growable list, somewhat like Java Vectors

#ifndef _VECTOR_H
#define _VECTOR_H

template <class T>
class Vector
{
   T *data;
   unsigned num_elt, next;
private:
   void setSize(unsigned i)
   {
      T *temp = new T[i];
      memcpy(temp, data, sizeof(T) * next);
      delete data;
      data = temp;
      num_elt = i;
   }
public:
   Vector(unsigned len = 10)
   {
      data = new T[len];
      num_elt = len;
      next = 0;
   }

   T& operator()(unsigned i) { return data[i]; }
   unsigned size() { return next; }
   void addElement(T thing)
   {
      if (next < num_elt)
         data[next++] = thing;
      else
      {
         setSize(num_elt * 2);
         addElement(thing);
      }
   }

   void kill() { delete data; }

   Vector<T> operator=(Vector<T> v)
   {
      data = v.data;
      num_elt = v.num_elt;
      next = v.next;
      return *this;
   }

   ~Vector() { }
};

template <class T>
inline Vector<T> copy(Vector<T> v)
{
   unsigned i;
   Vector<T> result;

   for (i = 0; i < v.size(); i++)
      result.addElement(v(i));

   return result;
}

template <class T>
inline Vector<T> copyBut(Vector<T> v, T exception)
{
   unsigned i;
   Vector<T> result;

   for (i = 0; i < v.size(); i++)
      if (v(i) != exception)
         result.addElement(v(i));

   return result;
}

template <class T>
inline void vectorReplace(Vector<T> v, T oldItem, T newItem)
{
   unsigned i;

   for (i = 0; i < v.size(); i++)
      if (v(i) == oldItem)
         v(i) = newItem;
   
}

template <class T>
inline void vectorAppend(Vector<T> & v1, Vector<T> v2)
{
   unsigned i;

   for (i = 0; i < v2.size(); i++)
      v1.addElement(v2(i));
}


#endif   /* !_VECTOR_H */
