// $Id: FormulaRegistry.h,v 1.3 2000/01/24 17:42:52 david Exp $

// FormulaRegistry.h
// Definitions for a formula registry to keep track of extra
//  generated formulae.  These can then be deleted at convenience
//  with the annhiliate() method.
// (Currently broker)

#ifndef _FORMULAREGISTRY_H
#define _FORMULAREGISTRY_H

#include "formula.h"
#include "Vector.h"

class FormulaRegistry
{
   Vector<PropFormula*> list; // growable list of formulae
   int killed;                // set to 1 if formulae already destroyed
public:
   FormulaRegistry() { killed = 0; }
   void addFormula(PropFormula *f) { list.addElement(f); }
   void annihilate();
};

#endif   /* !_FORMULAREGISTRY_H */

