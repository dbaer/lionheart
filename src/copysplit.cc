#include "copysplit.h"

using namespace std;

Vector<PropFormula*> copysplit(Vector<PropFormula*> v)
{
   Vector<PropFormula*> result;
   unsigned i;

   for (i = 0; i < v.size(); i++)
   {
      result.addElement(v(i)->formCopy());
   }

   return result;

}

Vector<PropFormula*> copysplitbut(Vector<PropFormula*> v, PropFormula *but)
{
   Vector<PropFormula*> result;
   unsigned i;

   for (i = 0; i < v.size(); i++)
   {
      if (v(i) != but)
         result.addElement(v(i)->formCopy());
   }

   return result;

}

