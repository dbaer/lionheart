// $Id: parser.h,v 1.3 2000/01/24 17:42:52 david Exp $

// parser.h
// by David Baer
//
// Headers for the parser

#ifndef _PARSER_H
#define _PARSER_H

#include <stdio.h>
#include "formula.h"

extern void initParser(istream&);
extern void initParser(char*);
extern PropFormula *parseFormula();
extern void endParser();

#endif /* !_PARSER_H */

