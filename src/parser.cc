// $Id: parser.cc,v 1.4 2000/01/25 03:05:43 david Exp $

// parser.cc
// by David Baer
//
// Convert text into a formula object

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include "lex.h"
#include "formula.h"
#include "Proof.h"
//#include "RuleSource.h"

Token current = (Token)-1;

inline void advance() { current = (Token)yylex(); }
inline void setup() {
//   while ((current = (Token)yylex()) == -1); // eat CRLF's
   while (current == -1 ) advance();
}

PropFormula *parseTerm();
PropFormula *parseAnd();
PropFormula *parseOr();
PropFormula *parseFormula();

PropFormula *build(PropFormula *left, Token op)
{
   //cout << "build" << endl;
   Token curOp = op;
   PropFormula *result = left;

   while (curOp == op)
   {
      advance(); // eat operator
      switch(op)
      {
         case TOKEN_IFF  : result = new BinOp(BINOP_IMPL, result, parseOr());
                           result = new BinOp(BINOP_AND, result, new BinOp(BINOP_IMPL, result->asBinOp()->rightOperand()->formCopy(), result->asBinOp()->leftOperand()->formCopy()));
                           break;
         case TOKEN_IMPL : result = new BinOp(BINOP_IMPL, result, parseOr());
                     break;
         case TOKEN_OR   : result = new BinOp(BINOP_OR, result, parseAnd());
                     break;
         case TOKEN_AND  : result = new BinOp(BINOP_AND, result, parseTerm());
                     break;
         default         : ;
      }
      curOp = current;
   }
   //cout << "done build" << endl;
   return result;
}

static inline
Vector<Var*> getVarList()
{
   Vector<Var*> result;
   
   //cout << "Variables in list: ";
   while (current == TOKEN_ID)
   {
      result.addElement(new Var(token_id_name));
      //cout << token_id_name;
      advance();

      if (current == TOKEN_COMMA)
      {
         advance();
         //cout << ", ";
      }
      //else cout << endl;
      
   }

   return result;
}

static inline
Var *handleId()
// pre: current -> TOKEN_ID
{
   Var *result;
   char *str = strdup(token_id_name);

   //cout << "handleId()" << endl;
   

   advance();
   if (current == TOKEN_LPAREN)
   {
      Vector<Var*> params;

      advance(); // eat (
      params = getVarList();
      if (current != TOKEN_RPAREN)
         cerr << "expected ) to close predicate parameter list" << endl;
      else advance(); // eat )
      
      result = new Pred(str, params);
      
   }
   else result = new Var(str);
   
   delete str;

   //cout << "handleId() result: ";
   //result->println(cout);
   return result;
}

static inline
QuantFormula *handleQuant()
// pre: current -> TOKEN_{FORALL,EXISTS}
{
   QuantifierType type = (current == TOKEN_FORALL ? QUANT_FORALL : QUANT_EXISTS);

   //cout << "handleQuant()" << endl;
   
   advance();
   // expect a { here
   if (current != TOKEN_LBRACE)
      // complain bitterly
      cerr << "expected a { after quantifier" << endl;
   else advance();

   Vector<Var*> vars = getVarList();
   
   if (current != TOKEN_RBRACE)
      cerr << "expected a } to close quantified variables" << endl;
   else advance(); // past } to quantified formula

   int i;
   PropFormula *formula = parseTerm(); // read quantified formula
   
   //cout << "prepending " << vars.size() << "variables" << endl;
   for (i = vars.size() - 1; i >= 0; i--)
      formula = new QuantFormula(type, vars(i), formula);

   //cout << "duh.." << endl;
   
   vars.kill();

   //cout << "handleQuant() result: ";
   //formula->println(cout);
   return formula->asQuant();
}

PropFormula *parseTerm()
{
   //cout << "parseTerm()" << endl;
   PropFormula *result;

   switch(current)
   {
      case -1 : cerr << "Premature end of file!" << endl;
                return NULL;
      case TOKEN_NOT : advance(); // eat ~
                       result = new Not(parseTerm());
                       break;
      case TOKEN_LPAREN : advance(); // eat (
                          result = parseFormula();
                          if (current != TOKEN_RPAREN)
                             cerr << "missing right paren!" << endl;
                          else advance(); // eat )
                          break;
      case TOKEN_ID : result = handleId(); break;
      case TOKEN_FORALL:
      case TOKEN_EXISTS: result = handleQuant(); break;
      default : return NULL;
   }

   //cout << "parseTerm() result: ";
   //result->println(cout);
   return result;
   
}

PropFormula *parseAnd()
{
   //cout << "parseAnd" << endl;
   PropFormula *result;

   switch(current)
   {
      case -1 : cerr << "Premature end of input!" << endl;
                return NULL;
      case TOKEN_NOT :
      case TOKEN_LPAREN :
      case TOKEN_FORALL:
      case TOKEN_EXISTS:
      case TOKEN_ID : result = parseTerm();
                      if (current == TOKEN_AND)
                         return build(result, TOKEN_AND);
                      else return result;
      default : return NULL;
   }
}

PropFormula *parseOr()
{
   //cout << "parseOr" << endl;
   PropFormula *result;

   switch(current)
   {
      case -1 : cerr << "Premature end of input!" << endl;
                return NULL;
      case TOKEN_NOT :
      case TOKEN_LPAREN :
      case TOKEN_FORALL:
      case TOKEN_EXISTS:
      case TOKEN_ID : result = parseAnd();
                      if (current == TOKEN_OR)
                         return build(result, TOKEN_OR);
                      else return result;
      default : return NULL;
   }
}

PropFormula *parseFormula()
{
   //cout << "parseFormula" << endl;
   PropFormula *result;

   switch (current)
   {
      case -1 : cerr << "Premature end of input!" << endl;
                return NULL;
      case TOKEN_NOT :
      case TOKEN_LPAREN :
      case TOKEN_FORALL:
      case TOKEN_EXISTS:
      case TOKEN_ID : // implication level
                      result = parseOr();
                      if ((current == TOKEN_IMPL) || (current == TOKEN_IFF))
                         return build(result, current);
                      else return result;
      default       : return NULL;
   }
}

void initParser(istream& i)
{
   //cout << "initParser" << endl;
   restart(i);
   //cout << "setup" << endl;
   setup();
   //cout << "done setup" << endl;
}

void initParser(char *str)
{
   restart(str);
   setup();
}

void endParser()
{
   flushInput();
}
