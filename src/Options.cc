/* $Id: Options.cc,v 1.6 2000/01/30 00:59:32 david Exp $ */

/* Options.cc
   by David Baer

   Parse command line options and convert to an object
*/

#include <iostream>
#include <string.h>
#include "Options.h"
#include "parser.h"
#include "Axiom.h"

#define APPNAME "lionheart"
#define MAJ_VER_NUM 0
#define MIN_VER_NUM "9f"

Options::Options(int argc, char *argv[])
{
   form = NULL;
   pause_mode = 0;
   man_mode = 0;
   output_file = NULL;
   quiet_mode = 1;
   help_req = 0;
   dump_mode = 0;
   max_param = 100;
   use_ind = 0;

   for (int i = 1; i < argc; i++)
      if (argv[i][0] != '-')
      {
         initParser(argv[i]);
         form = parseFormula();
         endParser();
      }
      else if (!strcmp(argv[i], "-ind"))
      {
         use_ind = 1;
      }
      else if (!strcmp(argv[i], "-pause"))
      {
         pause_mode = 1;
      }
      else if (!strcmp(argv[i], "-manual"))
      {
         man_mode = 1;
      }
      else if (!strncmp(argv[i], "-p", 2))
      {
         if (!(i < argc - 1) || !sscanf(argv[i + 1], "%u", &max_param))
            cerr << "warning: -p needs a numerical argument" << endl;
         i++;
      }
      else if (!strncmp(argv[i], "-o", 2))
      {
         if (i < argc - 1)
            output_file = strdup(argv[i + 1]);
         else cerr << "warning: -o needs an argument -- ignored" << endl;
         i++;
      }
      else if (!strncmp(argv[i], "-v", 2))
      {
         quiet_mode = 0;
      }
      else if (!strcmp(argv[i], "-dump"))
      {
         dump_mode = 1;
      }
      else if (!strcmp(argv[i], "-a"))
      {
         if (i < argc - 1)
         {
            char *axfiles = strdup(argv[i + 1]),
               *curFile = strtok(axfiles, ",");
            Vector<char*> files;

            while (curFile)
            {
               files.addElement(curFile);
               curFile = strtok(NULL, ",");
            }

            for (unsigned j = 0; j < files.size(); j++)
               axioms.addAxioms(files(j));

            files.kill();

            delete axfiles;
         }
         else cerr << "warning: -a needs an argument -- ignored" << endl;
         i++;
      }
      else help_req = 1;
}

void Options::syntax()
{
   cout << APPNAME << " " << MAJ_VER_NUM << "." << MIN_VER_NUM << endl;
   cout << endl;
   cout << "syntax: " << APPNAME << " [-o <outputfile>] [-q] [-manual] [-dump] [<formula>]" << endl;
   cout << endl;
   cout << "<formula>       : the propositional formula to be proved" << endl;
   cout << "-a <file1>[,<file2>,...] : axiom files" << endl;
   cout << "-o <outputfile> : output proof to this text file" << endl;
   cout << "-p #            : maximum parameter number (default: 100)" << endl;
   cout << "-manual         : prompt user for proof steps" << endl;
   cout << "-v              : verbose mode -- all machine steps displayed " << endl;
   cout << "-dump           : dump proof to standard output" << endl;
   cout << endl;
}
