// $Id: Rule.h,v 1.7 2000/01/30 00:59:32 david Exp $

// Rule.h
// Abstraction type for refinement-logic rules

#ifndef _RULE_H
#define _RULE_H

#include "formula.h"
#include <iostream>

// types of rules -- the standard refinement logic set plus a few
//                   pseudo-rules
typedef enum
{
   AND_L,
   AND_R,
   OR_L,
   OR_R1,
   OR_R2,
   OR_R,
   IMP_L,
   IMP_R,
   NOT_L,
   NOT_R,
   CUT,
   MAGIC,
   HYP,
   INSTANT,
   IND,
   AXIOM,
   GIVEUP,      // exit proof
   BACKUP_RULE  // backup to last subgoal
} RuleType;

// rule names -- used for printing
extern const char *rulename[];

class Proof; // used in class Rule

class Rule
{
   RuleType rule;
   PropFormula *target;
   PropFormula *indZero, *indSucc;
   Proof *pf;
   Var *var;
   int arg;
   char *axname;
public:
   Rule()
   {
      rule = AND_L;
      target = indZero = indSucc = NULL;
      arg = 2;
      pf = NULL;
      var = NULL;
   }
   Rule(RuleType r, PropFormula *t, int a=-1, Proof *p = NULL)
   {
      rule = r;
      target = t;
      arg = a;
      pf = p;
      var = NULL;
      indZero = indSucc = NULL;
   }
   Rule(RuleType r, PropFormula *t, char *name)
   {
      rule = r;
      axname = strdup(name);
      pf = NULL;
      target = t;
      var = NULL;
      indZero = indSucc = NULL;
   }

   // constructor for induction
   Rule(RuleType r, PropFormula *iZ, PropFormula *iS)
   {
      rule = r;
      indZero = iZ;
      indSucc = iS;
      pf = NULL;
      target = NULL;
      var = NULL;
   }

   void setArg(int a) { arg = a; }
   char *getName() { return axname; }
   
   Var *getVar() { return this->var; }
   PropFormula *getTarget() { return target; }
   PropFormula *getInductionZero() { return indZero; }
   PropFormula *getInductionSucc() { return indSucc; }
   RuleType getRule() { return rule; }
   Rule& operator=(Rule r)
   {
      rule = r.rule;
      target = r.target;
      pf = r.pf;
      arg = r.arg;
      axname = r.axname;
      indZero = r.indZero;
      indSucc = r.indSucc;

      return *this;
   }
   int argument() { return arg; }
   virtual boolean isProof() 
      {
         return pf != NULL;
      }
   virtual Proof *getProof()
      {
         return pf;
      }
};

#endif   /* !_RULE_H */
