// $Id: RuleSource.h,v 1.5 2000/01/30 00:59:32 david Exp $

// RuleSource.h
// An abstraction used for analyzing sequents and supplying rules.

#ifndef _RULESOURCE_H
#define _RULESOURCE_H

#include <iostream>
#include "Rule.h"
#include "formula.h"
#include "Vector.h"

using namespace std;

// all we need in the abstract class is a method for returning rules
class RuleSource
{
public:
   virtual Rule getRule(Vector<PropFormula*>, PropFormula*)=0;
};

// A user-assisted rule source -- done
class UserRuleSource : virtual public RuleSource
{
protected:
   istream *istr;   // input stream
   ostream *ostr;   // output stream
public:
   UserRuleSource(istream &i, ostream &o) { istr = &i; ostr = &o; }
   Rule getRule(Vector<PropFormula*>, PropFormula*);
private:
   unsigned getFormula(Vector<PropFormula*>, Vector<unsigned>);
   unsigned getInstantParam();
};

class AutoRuleSource : virtual public UserRuleSource
{
   unsigned quiet, pause, induct;
public:
   AutoRuleSource(istream &i = cin, ostream &o = cout, unsigned q = 0, unsigned p = 0, unsigned in = 0) : UserRuleSource(i, o) { quiet = q; pause = p; induct = in; } 
   Rule getRule(Vector<PropFormula*>, PropFormula*);
};

#endif   /* !_RULESOURCE_H */

