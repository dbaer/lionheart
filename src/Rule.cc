// $Id: Rule.cc,v 1.4 2000/01/29 22:33:53 david Exp $

// Rule.cc
// Sadly, there is nothing here but the names of the rules.

const char *rulename[] = { "andL",
                           "andR",
                           "orL",
                           "orR1",
                           "orR2",
                           "orR",
                           "impL",
                           "impR",
                           "notL",
                           "notR",
                           "cut",
                           "magic",
                           "hyp",
                           "instantiation",
                           "induction",
                           "axiom",
                           "giveup",
                           "backup" };

