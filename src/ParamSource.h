/* $Id: ParamSource.h,v 1.4 2000/01/25 03:05:43 david Exp $ */

/* ParamSource.h
   by David Baer

   Generate new (unique) parameter names
*/

#ifndef _PARAMSOURCE_H
#define _PARAMSOURCE_H

#include "formula.h"

class ParamSource
{
   unsigned count, max;
 public:
   ParamSource() { count = 0; }

   Var *freshParam()
   {
      char str[20];
      snprintf(str, 20, "p%u", count++);
      
      return new Var(str);
   }

   void setNext(unsigned n) { if (n > count) count = n; }
   unsigned viewNext() { return count; }
   void setMax(unsigned m) { max = m; }
   unsigned getMax() { return max; }
};

extern ParamSource psource;

#endif /* !def _PARAMSOURCE_H */
