// $Id: lex.cc,v 1.3 2000/01/24 17:42:52 david Exp $

// lex.cc
// by David Baer
//
// Lexical analyzer -- helps to parse formulas

#include <iostream>
#include <cstdlib>
#include <ctype.h>
#include <cstring>
//#include <bsd/string.h>
#include "lex.h"

using namespace std;

char *token_id_name = NULL;
istream *input;

void setInput(istream& i)
{
   input = &i;
}

// private class, local to this file
class LexBuffer
{
   char buf[256];
   int pos, end, str;
public:
   LexBuffer() { pos = 0; end = -1; str = 0; do advance(); while (!strlen(buf)); }
   LexBuffer(char *string) { strlcpy(buf, string, 256); pos = 0; end = strlen(string); str = 1; }
   void advance() { 
         //cout << "buffer.advance" << endl;
      if (!str && (pos > end))
      {
                  //cout << "buffer.advance read" << endl;
         input->getline(buf, 255);
                  //cout << "got line: \"" << buf << "\"" << endl;
         pos = 0;
         end = input->gcount();
      }
      else if (pos <= end) pos++;
               //cout << "buffer.advance done: pos = " << pos << ", end = " << end << endl;
   }
   char current() {
      if (pos >= end) return '\0';
      //cout << "buffer.current = " << buf[pos] << endl;
      return buf[pos];
   }
} *buffer;

void setInput(char *str)
{
   buffer = new LexBuffer(str);
}

void illChar(char ch)
{
   cerr << "Illegal character ('" << ch << "') in input!" << endl;
   exit(0);
}

void killStr()
{
   delete token_id_name;
}

int yylex(void)
{
   if (!buffer) buffer = new LexBuffer();
   char ch, idBuf[500];

   // eat whitespace
   while ((ch = buffer->current()) == ' ') buffer->advance();

   switch (ch)
   {
      case '\r': buffer->advance();
      case '\n': buffer->advance();
      case '\0': /* cout << "end: " << (int)ch << endl; */ return -1;
      case ',' : buffer->advance(); return TOKEN_COMMA;
      case '{' : buffer->advance(); return TOKEN_LBRACE;
      case '}' : buffer->advance(); return TOKEN_RBRACE;
      case '(' : buffer->advance(); return TOKEN_LPAREN;
      case ')' : buffer->advance(); return TOKEN_RPAREN;
      case '&' : buffer->advance(); return TOKEN_AND;
      case '|' : buffer->advance(); return TOKEN_OR;
      case '=' : while (buffer->current() != '>') 
                 {
                    if (buffer->current() != '=')
                    {
                       illChar(buffer->current());
                    }
                    buffer->advance();
                 }
                 buffer->advance(); // eat final >
                 return TOKEN_IMPL;
      case '<' : buffer->advance();

                 while (buffer->current() != '>')
                 {
                    if (buffer->current() != '=')
                       illChar(buffer->current());
                    buffer->advance();
                 }
                 buffer->advance(); // eat final >
                 return TOKEN_IFF;
      case '~' : buffer->advance(); return TOKEN_NOT;
      default  : if (!isalpha(ch))
                    illChar(buffer->current());
                 idBuf[0] = ch;
                 int index = 1;
                 buffer->advance();
                 while (isalpha(ch = buffer->current()))
                 {
                    idBuf[index++] = ch;
                    buffer->advance();
                 }
                 idBuf[index] = '\0';
                 if (token_id_name) delete token_id_name;
                 token_id_name = strdup(idBuf);
                 if (!strcasecmp(token_id_name, "FORALL")) return TOKEN_FORALL;
                 else if (!strcasecmp(token_id_name, "EXISTS")) return TOKEN_EXISTS;
                 else return TOKEN_ID;
   }

}

void flushInput() {
//   char line[100];
//   input.getline(line, 98);
   if (buffer)
      {
         delete buffer;
         buffer = NULL;
      }
   
}

static
const char *token_name[] =
{
   "TOKEN_COMMA",
   "TOKEN_FORALL",
   "TOKEN_EXISTS",
   "TOKEN_LBRACE",
   "TOKEN_RBRACE",
   "TOKEN_LPAREN",
   "TOKEN_RPAREN",
   "TOKEN_AND",
   "TOKEN_OR",
   "TOKEN_IMPL",
   "TOKEN_NOT",
   "TOKEN_ID"
};

void printToken(Token t)
{
   if ((t >= TOKEN_COMMA) && (t <= TOKEN_ID))
      cout << token_name[(int)t];
   else cout << "(UNKNOWN TOKEN)";
}


