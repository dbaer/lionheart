// $Id: lex.h,v 1.3 2000/01/24 17:42:52 david Exp $

// lex.h -- contains token types
#ifndef _LEX_H
#define _LEX_H

#include <iostream>
using std::istream;

typedef enum {
   TOKEN_ERR = -1,
   TOKEN_COMMA = 0,
   TOKEN_FORALL,
   TOKEN_EXISTS,
   TOKEN_LBRACE,
   TOKEN_RBRACE,
   TOKEN_LPAREN,
   TOKEN_RPAREN,
   TOKEN_AND,
   TOKEN_OR,
   TOKEN_IMPL,
   TOKEN_IFF,
   TOKEN_NOT,
   TOKEN_ID } Token;

#define restart setInput

extern char *token_id_name;
extern void setInput(istream&);
extern void setInput(char*);
extern int yylex(void);
extern void flushInput();
extern void printToken(Token);

#endif   /* !_LEX_H */

