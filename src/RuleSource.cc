// $Id: RuleSource.cc,v 1.10 2000/01/30 00:59:32 david Exp $

// RuleSource.cc
// routines for supplying rules for refinement logic proofs

#include <string.h>
#include "RuleSource.h"
#include "parser.h"
#include "Proof.h"
#include "ParamSource.h"
#include "Axiom.h"

#ifndef NULL
#define NULL 0
#endif /* !_NULL */

using namespace std;

// When the user wants to apply a left-rule, sometimes there is more
//  than one possible target.  This routine lists all the
//  possibilities and prompts the user for his/her choice.
unsigned UserRuleSource::getFormula(Vector<PropFormula*> v, Vector<unsigned> possible)
{
   unsigned result, done = 0, i;

   while (!done)
   {
      for (i = 0; i < possible.size(); i++)
      {
         *ostr << possible(i) + 1 << ") ";
         v(possible(i))->println(*ostr);
      }
      *ostr << "which formula on left? ";
      *istr >> result;
      for (i = 0; !done && (i < possible.size()); i++)
         done = (result == (possible(i) + 1));
   }

   char nullstr[20];
   
   istr->getline(nullstr, 18);
   

   return result - 1;
}

unsigned UserRuleSource::getInstantParam()
{
   char instring[100];
   unsigned num;

   *ostr << "enter a parameter for instantiation: p";

   istr->getline(instring, 98);
   
   sscanf(instring, "%u", &num);

   psource.setNext(num + 1); // ensure freshness for fresh parameters

   return num;
}

Rule UserRuleSource::getRule(Vector<PropFormula*> l, PropFormula *r)
{
   char inbuf[100];

   while (1) // get good rule
   {
      for (unsigned i = 0; i < l.size(); i++)
      {
         l(i)->print(*ostr);
         if (i < l.size() - 1)
            *ostr << ", ";
      }
      *ostr << " |- ";
      r->print(*ostr);
      *ostr << " by ";
      istr->getline(inbuf, 98);
      if (!strcasecmp(inbuf, "andr"))
      {
         Rule result(AND_R, NULL);
         return result;
      }
      else if (!strcasecmp(inbuf, "orr1"))
      {
         Rule result(OR_R1, NULL);
         return result;
      }
      else if (!strcasecmp(inbuf, "orr2"))
      {
         Rule result(OR_R2, NULL);
         return result;
      }
      else if (!strcasecmp(inbuf, "orr"))
      {
         Rule result(OR_R, NULL);
         return result;
      }
      else if (!strcasecmp(inbuf, "impr"))
      {
         Rule result(IMP_R, NULL);
         return result;
      }
      else if (!strcasecmp(inbuf, "notr"))
      {
         Rule result(NOT_R, NULL);
         return result;
      }
      else if (!strcasecmp(inbuf, "andl"))
      {
         if (l.size() == 0) continue; // nothing on lhs!
         unsigned which;
         Vector<unsigned> possible;
         for (which = 0; which < l.size(); which++)
            if (l(which)->isBinOp() && (l(which)->asBinOp()->op() == BINOP_AND))
               possible.addElement(which);
         if (possible.size() == 0)
            which = 0;
         else if (possible.size() == 1)
            which = possible(0);
         else which = getFormula(l, possible);
   
         Rule result(AND_L, l(which), which);
         return result;
      }
      else if (!strcasecmp(inbuf, "orl"))
      {
         if (l.size() == 0) continue; // nothing on lhs!
         unsigned which;
         Vector<unsigned> possible;
         for (which = 0; which < l.size(); which++)
            if (l(which)->isBinOp() && (l(which)->asBinOp()->op() == BINOP_OR))
               possible.addElement(which);
         if (possible.size() == 0)
            which = 0;
         else if (possible.size() == 1)
            which = possible(0);
         else which = getFormula(l, possible);

         Rule result(OR_L, l(which), which);
         return result;
      }
      else if (!strcasecmp(inbuf, "impl"))
      {
         if (l.size() == 0) continue; // nothing on lhs!
         unsigned which;
         Vector<unsigned> possible;
         for (which = 0; which < l.size(); which++)
            if (l(which)->isBinOp() && (l(which)->asBinOp()->op() == BINOP_IMPL))
               possible.addElement(which);
         if (possible.size() == 0)
            which = 0;
         else if (possible.size() == 1)
            which = possible(0);
         else which = getFormula(l, possible);
   
         Rule result(IMP_L, l(which), which);
         return result;
      }
      else if (!strcasecmp(inbuf, "notl"))
      {
         if (l.size() == 0) continue; // nothing on lhs!
         unsigned which;
         Vector<unsigned> possible;
         for (which = 0; which < l.size(); which++)
            possible.addElement(which);
         if (possible.size() == 0)
            which = 0;
         else if (possible.size() == 1)
            which = possible(0);
         else which = getFormula(l, possible);
   
         Rule result(NOT_L, l(which), which);
         return result;
      }
      else if (!strcasecmp(inbuf, "instant"))
      {
         if (l.size() == 0) continue;
         unsigned which;
         Vector<unsigned> possible;
         for (which = 0; which < l.size(); which++)
            if (l(which)->isQuant())
               possible.addElement(which);
         if (possible.size() == 0)
            which = 0;
         else if (possible.size() == 1)
            which = possible(0);
         else which = getFormula(l, possible);

         /*Var *p = (l(which)->asQuant()->getType() == QUANT_EXISTS ? NULL : getInstantParam());*/
         unsigned param;
         if (l(which)->isQuant())
            param = (l(which)->asQuant()->getType() == QUANT_EXISTS ? 0 : getInstantParam());
         else param = 0;

         /*if (!p)
           cout << "p == NULL" << endl;*/
         
         Rule result(INSTANT, l(which), param);
         
         return result;
      }
      else if (!strcasecmp(inbuf, "ind"))
      {
         if (!r->isQuant() || (r->asQuant()->getType() != QUANT_FORALL))
            continue;

         char zeroStr[128], succStr[128];

         *ostr << "zero(i) <==> " << flush;
         *istr >> zeroStr;
         initParser(zeroStr);
         PropFormula * zero_pred = parseFormula();
         endParser();

         *ostr << "succ(i, j) <==> " << flush;
         *istr >> succStr;
         initParser(succStr);
         PropFormula * succ_pred = parseFormula();
         endParser();

         *ostr << "Thanks!" << endl;

         Rule result(IND, zero_pred, succ_pred);

         /**ostr << "zero: " << flush;
         result.getInductionZero()->println(*ostr);
         *ostr << "succ: " << flush;
         result.getInductionSucc()->println(*ostr);*/

         return result;
      }
      else if (!strcasecmp(inbuf, "axiom"))
      {
         axioms.printAxioms(*ostr);
         *ostr << "which one? " << flush;
         char name[40];
         istr->getline(name, 38);
         PropFormula *ax = axioms.getAxiom(name);

         *ostr << "New axiom: " << flush;
         ax->println(*ostr);

         if (ax)
         {
            Rule result(AXIOM, ax, name);
            return result;
         }
         else continue;
      }
      else if (!strcasecmp(inbuf, "hyp"))
      {
         Rule result(HYP, NULL);
         return result;
      }
      else if (!strcasecmp(inbuf, "giveup"))
      {
         Rule result(GIVEUP, NULL);
         return result;
      }
      else if (!strcasecmp(inbuf, "cut"))
      {
         *ostr << "Enter formula: " << flush;
         initParser(*istr);
         Rule result(CUT, parseFormula());
         endParser();
         return result;
      }
      else if (!strcasecmp(inbuf, "magic"))
      {
         Rule result(MAGIC, NULL);
         return result;
      }
      else if (!strcasecmp(inbuf, "backup"))
      {
         Rule result(BACKUP_RULE, NULL);
         return result;
      }
      else
      {
         Rule result(AND_R, NULL);
         return result;
      }
   }
}


/*int order[] = { 1, 4, 3, 6, 7, 5, 2, 8, 9, 10, 11, 12, 13 };*/
PropFormula *basicTarget = NULL;

unsigned isBasic(Vector<PropFormula*>list)
{
   unsigned i, finalresult = 1, result = 1;

   //cout << "checking list..." << endl;
   basicTarget = NULL;

   for (i = 0; i < list.size(); i++)
   {
      //list(i)->println(cout);
      unsigned temp = list(i)->isNot() &&
         list(i)->asNot()->notOperand()->isBinOp() &&
         (list(i)->asNot()->notOperand()->asBinOp()->op() == BINOP_OR);
      result = list(i)->isVar() || list(i)->isQuant() ||
         list(i)->isNot() && list(i)->asNot()->notOperand()->isVar() ||
         temp;
      
      if (result && temp && (basicTarget == NULL))
         basicTarget = list(i);
      else if (!result)
         if (!basicTarget)
            basicTarget = list(i);
         else if (basicTarget->isBinOp() && list(i)->isBinOp() &&
                  ((unsigned)basicTarget->asBinOp()->op() > (unsigned)list(i)->asBinOp()->op()))
            basicTarget = list(i);
      finalresult &= result;
   }

   //cout << "done, list is" << (finalresult ? " " : " not ") << "basic" << endl;
   

   return finalresult;
}

Rule AutoRuleSource::getRule(Vector<PropFormula*>lhs, PropFormula *rhs)
{
   unsigned i, basic_lhs = isBasic(lhs);
   RuleType result = GIVEUP;
   PropFormula *target = NULL;
   Proof *pftarget = NULL;
   PropFormula *zero = NULL, *succ = NULL;

   if (rhs->isVar() || rhs->isQuant())
   {
      for (i = 0; i < lhs.size(); i++)
         if (lhs(i)->equals(rhs))
            result = HYP;
      if (result == GIVEUP)
      {
         result = NOT_R;
         if (induct && rhs->isQuant() && (rhs->asQuant()->getType() == QUANT_FORALL))
         {
            Vector<Var*> v;
            v.addElement(new Var("i"));
            zero = new BinOp(BINOP_AND, new Pred("Integer", v), new Pred("Zero", v));
            v.addElement(new Var("j"));
            Vector<Var*> w;
            w.addElement(new Var("i"));
            succ = new BinOp(BINOP_AND, new Pred("Integer", w), new Pred("Succ", v));

            result = IND;
         }
      }
   }

   if ((result == GIVEUP) && rhs->isBinOp())
   {
      if (rhs->asBinOp()->op() == BINOP_AND)
         result = AND_R;
      if (rhs->asBinOp()->op() == BINOP_IMPL)
         result = IMP_R;
      if (rhs->asBinOp()->op() == BINOP_OR)
         result = OR_R;

      /* This code is commented out because it used the standard  */
      /* refinement-logic ORR1 and ORR2, which gets clumsy if you */
      /* can't guess which to use, and harmful as in the case of  */
      /* p|~p.                                                    */
      /*
      {
         PropFormula *left = rhs->asBinOp()->leftOperand(),
            *right = rhs->asBinOp()->rightOperand();

         if ((left->isNot() && (left->asNot()->notOperand()->equals(right))) ||
             (right->isNot() && (right->asNot()->notOperand()->equals(left))))
            result = MAGIC;
      }
      */
   }

   if ((result == GIVEUP) && rhs->isNot())
   {
      result = NOT_R;
   }

   /* The new code will never get this far.  OR is handled above. */
   /*
   if ((result == GIVEUP) &&
       rhs->isBinOp() &&
       (rhs->asBinOp()->op() == BINOP_OR))
   {
      if (basic_lhs)
      {
         PropFormula *new_rhs = rhs->asBinOp()->leftOperand()->formCopy();

         new_rhs->setParent(rhs);

         Proof *attempt = new Proof(lhs, new_rhs, this);
         if (attempt->prove() == PROOF_TRUE)
         {
            pftarget = attempt;
            target = rhs;
            result = OR_R1;
         }
         if (result == GIVEUP)
         {
            delete attempt; // lhs of or was unsuccessful
            
            new_rhs = rhs->asBinOp()->rightOperand()->formCopy();
            new_rhs->setParent(rhs);
         
            attempt = new Proof(lhs, new_rhs, this);
            if (attempt->prove() == PROOF_TRUE)
            {
               pftarget = attempt;
               target = rhs;
               result = OR_R2;
            }
            if (result == GIVEUP)
               delete attempt; // rhs of or was unsuccessful
         }
      }
      if (result == GIVEUP)
      {
         //if (basicTarget->isNot())
         //{
         //   target = basicTarget;
         //   result = NOT_L;
         //}
         if (!basic_lhs && rhs->isBinOp() && (rhs->asBinOp()->op() == BINOP_OR))
         {
            result = NOT_R;
         }
         
         if (basicTarget)
         {
            
            if (basicTarget->isBinOp() && (basicTarget->asBinOp()->op() == BINOP_OR))
            {
               target = basicTarget;
               result = OR_L;
            }
         
            if (basicTarget->isBinOp() && (basicTarget->asBinOp()->op() == BINOP_AND))
            {
               target = basicTarget;
               result = AND_L;
            }
         
            if (basicTarget->isBinOp() && (basicTarget->asBinOp()->op() == BINOP_IMPL))
            {
               target = basicTarget;
               result = IMP_L;
            }
         }
         
      }
   }
   */
   
   if ((result == GIVEUP) &&
       rhs->isConst())
   {
      if (basic_lhs && basicTarget)
      {
         target = basicTarget;
         //cout << "boo!" << basic_lhs << endl;
         result = NOT_L;
      }
      else if (!basic_lhs && basicTarget)
      {
         if (basicTarget->isBinOp())
         {
            if (basicTarget->asBinOp()->op() == BINOP_AND)
            {
               target = basicTarget;
               result = AND_L;
            }
            if (basicTarget->asBinOp()->op() == BINOP_OR)
            {
               target = basicTarget;
               result = OR_L;
            }

            if (basicTarget->asBinOp()->op() == BINOP_IMPL)
            {
               target = basicTarget;
               result = IMP_L;
            }
         }
         if (basicTarget->isNot())
         {
            target = basicTarget;
            result = NOT_L;
         }
         
      }
   }

   if (result == GIVEUP)
   {
      unsigned j;

      for (i = 0; (result == GIVEUP) && (i < lhs.size()); i++)
      {
         for (j = i + 1; (result == GIVEUP) && (j < lhs.size()); j++)
            if (lhs(i)->isNot() &&
                lhs(i)->asNot()->notOperand()->isVar() &&
                lhs(j)->isVar() &&
                lhs(i)->asNot()->notOperand()->equals(lhs(j)))
            {
               target = lhs(i);
               result = NOT_L;
            }
            else if (lhs(j)->isNot() &&
                     lhs(j)->asNot()->notOperand()->isVar() &&
                     lhs(i)->isVar() &&
                     lhs(j)->asNot()->notOperand()->equals(lhs(i)))
            {
               target = lhs(j);
               result = NOT_L;
            }
      }
   }

   if (result == GIVEUP)
   {
      for (i = 0; (result == GIVEUP) && (i < lhs.size()); i++)
      {
         if (lhs(i)->isQuant() &&
             (lhs(i)->asQuant()->getType() == QUANT_EXISTS))
         {
            target = lhs(i);
            if (psource.viewNext() <= psource.getMax())
               result = INSTANT;
            else result = GIVEUP;
         }
      }
   }

   if (result == GIVEUP)
   {
      unsigned min = 0;
      target = NULL;

      for (i = 0; i < lhs.size(); i++)
         if (lhs(i)->isQuant() &&
             (lhs(i)->asQuant()->getType() == QUANT_FORALL))
         {
            if ((lhs(i)->asQuant()->nextParam() < min) || !target)
            {
               target = lhs(i);
               result = INSTANT;
               min = lhs(i)->asQuant()->nextParam();
            }
         }
      if ((result == INSTANT) && (min > psource.getMax()))
         result = GIVEUP;
   }

   if (result == GIVEUP)
   {
      // don't prompt user -- GIVEUP serves a useful purpose, esp. with OR
      // return UserRuleSource::getRule(lhs, rhs);
   }

   if (!quiet)
   {
      for (i = 0; i < lhs.size(); i++)
      {
         lhs(i)->print(*ostr);
         if (i < lhs.size() - 1) *ostr << ", " << flush;
      }
      *ostr << " |- ";
      rhs->print(*ostr);
      *ostr << " by " << rulename[(unsigned)result] << flush << endl;
      if (pause)
      {
         char strbuf[100];
         *istr >> strbuf;
      }
   }
   if (!pftarget && (result != IND))
      if ((result == INSTANT) && (target->asQuant()->getType() == QUANT_FORALL))
      {
         if (target->asQuant()->nextParam() > psource.getMax())
            result = GIVEUP;

         Rule r(result, target, target->asQuant()->nextParam());
         return r;
      }
      else return Rule(result, target);
   else if (result == IND)
      return Rule(IND, zero, succ);
   else return Rule(result, target, -1, pftarget);
}

